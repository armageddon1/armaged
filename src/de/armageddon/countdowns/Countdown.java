package de.armageddon.countdowns;

import java.util.Collections;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import de.armageddon.Gamestate;
import de.armageddon.Main;
import de.armageddon.Scoreboard;
import de.armageddon.Utils;
import de.armageddon.CMD.Cmd_feedback;
import de.armageddon.Listener.KickListener;
import de.armageddon.Manager.BannerSetter;
import de.armageddon.Manager.KitManager;
import de.armageddon.Manager.LocationManager;
import de.armageddon.Manager.TeamIdentifier;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.IChatBaseComponent;
import net.minecraft.server.v1_8_R1.PacketPlayOutChat;

public class Countdown {

	public LocationManager lm = new LocationManager();

	public boolean restartstarted = false;
	public int lobbycd;
	public int gamecd;
	int restartcd;
	int respawncd;
	BukkitTask reloadsnowball;

	public static int lobby1 = Main.main.seklobby;
	public int lobby = lobby1;
	int restart = Main.main.sekrestart;
	int ingame = Main.main.sekingame;
	int time = Main.main.respawntime;
	int low = 0;
	int high = 3;
	int sched;
	public int reload = 5;
	int playerstats;
	public boolean countdownstarted = false;

	KitManager km = new KitManager();

	BannerSetter bs = new BannerSetter();

	Random rnd = new Random();

	Scoreboard score = new Scoreboard();

	Utils utils = new Utils();
	TeamIdentifier ti = new TeamIdentifier();
	KickListener kl = new KickListener();
	Cmd_feedback fb = new Cmd_feedback();


	
	
	/*
	 * public void startRespawnCD(Player victim) { time = Main.main.respawntime;
	 * respawncd = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.main,
	 * new Runnable() {
	 * 
	 * @Override public void run() { if(time >= 2) {
	 * victim.sendMessage(Main.main.pr +
	 * Main.main.teleportrespawnsekunden.replace("{remain}", "" + time)); } else
	 * if(time == 1) { victim.sendMessage(Main.main.pr +
	 * Main.main.teleportrespawnsekunde.replace("{remain}", "1")); } else
	 * if(time == 0) { if(Main.main.teamblau.contains(victim)) {
	 * victim.teleport(Main.main.lm.getSpawn(0)); //Inventar neu bekommen
	 * Main.main.respawntime = 10; victim.sendMessage(Main.main.pr +
	 * Main.main.teleportrespawn); } else if(Main.main.teamrot.contains(victim))
	 * { victim.teleport(Main.main.lm.getSpawn(1)); //Inventar neu bekommend
	 * Main.main.respawntime = 10; victim.sendMessage(Main.main.pr +
	 * Main.main.teleportrespawn); } } time--; } }, 0, 20);
	 * 
	 * }
	 */

	public void startReloadSnowball() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.main, new Runnable() {

			@Override
			public void run() {

				for (int i = 0; i < Main.main.reloadsnowball.size(); i++) {
					Player s = Main.main.reloadsnowball.get(i);
					Main.main.respawning.remove(s);
				}

			}

		}, 0, 2 * 60 * 20);
	}

	public void playerStats() {
		playerstats = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.main, new Runnable() {

			@Override
			public void run() {
				Main.main.reloadConfig();
				lm.savePlayers();

			}

		}

				, 20, 20);
	}

	public void startGameCD() {
		Main.main.state = Gamestate.INGAME;
		for (Player all : Bukkit.getOnlinePlayers()) {
			lm.addPlayers(all);
			ti.giveChestplate(all);
			ti.setScoreboard(all);
			ti.broadcastTeam(all);
		}
		lm.savePlayers();
		for (Player all : Bukkit.getOnlinePlayers()) {
			ti.giveChestplate(all);
		}
		playerStats();
		gamecd = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.main, new Runnable() {

			@Override
			public void run() {

				for (Player all : Bukkit.getOnlinePlayers()) {
					ti.setScoreboard(all);
				}
				if (Main.main.sekingame == 0) {
					if (Main.main.teambluepoints == 0 || Main.main.teamredpoints == 0) {
						Bukkit.broadcastMessage(
								Main.main.pr + "Das Spiel ist unentschieden, da kein Team einen Punkt gemacht hat.");
					}
					if (Main.main.teamredpoints == 1 && Main.main.teambluepoints == 1) {
						Bukkit.broadcastMessage(
								Main.main.pr + "Das Spiel ist unentschieden, da beide Teams 1 Punkt erzielt haben.");
					} else if (Main.main.teambluepoints == 1 || Main.main.teamredpoints == 1) {
						if (Main.main.teamredpoints == 1) {
							Bukkit.broadcastMessage(Main.main.pr + "Team " + ChatColor.RED + " Rot" + ChatColor.GRAY
									+ " hat mit einem Punkt gewonnen!");
						} else if (Main.main.teambluepoints == 1) {
							Bukkit.broadcastMessage(Main.main.pr + "Team " + ChatColor.BLUE + " Blau" + ChatColor.GRAY
									+ " hat mit einem Punkt gewonnen!");
						}
					}
					endGame();
					Bukkit.getScheduler().cancelTask(gamecd);
					Bukkit.getScheduler().cancelTask(gamecd);
					Bukkit.getScheduler().cancelTask(gamecd);
					Bukkit.getScheduler().cancelTask(gamecd);
					Bukkit.getScheduler().cancelTask(gamecd);
					Bukkit.getScheduler().cancelTask(gamecd);
					Bukkit.getScheduler().cancelTask(gamecd);
					Bukkit.getScheduler().cancelTask(gamecd);
					Bukkit.getScheduler().cancelTask(gamecd);

					Bukkit.getScheduler().cancelTask(gamecd);
					Bukkit.getScheduler().cancelTask(gamecd);
					Bukkit.getScheduler().cancelTask(gamecd);

					// tp lobby
				}
				for (Player all : Bukkit.getOnlinePlayers()) {
					score.createScoreboard(all);
				}
				Main.main.sekingame--;
			}
		}, 0, 20);
	}

	public void startRestartCD() {
		Main.main.state = Gamestate.RESTART;
		if (restartstarted == false) {
			restartstarted = true;
			for (Player all : Bukkit.getOnlinePlayers()) {
				kl.giveKickItem(all);
			}
			int totalMinutes = Main.main.sekrestart / 60;
			int minutes = totalMinutes % 60;
			Bukkit.broadcastMessage("�cDer Server restartet in �6" + minutes + " �cMinuten!");
			fb.broadcast();
			restartcd = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.main, new Runnable() {

				@Override
				public void run() {
					if (Main.main.sekrestart >= 1) {
						if (Main.main.sekrestart == 60 || Main.main.sekrestart == 30 || Main.main.sekrestart == 15
								|| Main.main.sekrestart <= 5 && Main.main.sekrestart >= 1) {
							if (Main.main.sekrestart == 1) {
								Bukkit.broadcastMessage(
										"�cDer Server restartet in �6" + Main.main.sekrestart + " �cSekunde");
							} else {
								Bukkit.broadcastMessage(
										"�cDer Server restartet in �6" + Main.main.sekrestart + " �cSekunden");
							}
						}
					} else if (Main.main.sekrestart == 0) {

						for (Player all : Bukkit.getOnlinePlayers()) {
							all.kickPlayer("�4Server startet neu!");
						}
						Main.main.getServer().shutdown();
						Bukkit.getScheduler().cancelTask(restartcd);

					}
					for (Player all : Bukkit.getOnlinePlayers()) {
						all.setLevel(Main.main.sekrestart);
						all.setExp((float) (lobby * 0.01667));
						// restart starten (Serverseitig)
					}
					Main.main.sekrestart--;
				}

			}, 20, 20);
		}
	}

	@Deprecated
	public BukkitTask getreloadSnowBallTask(Player p) {
		utils.clearPlayer(p);
		reloadsnowball = Bukkit.getScheduler().runTaskTimerAsynchronously(Main.main, new Runnable() {
			@Override
			public void run() {
				if (!Main.main.reloadsnowball.contains(p)) {
					Main.main.reloadsnowball.add(p);
					if (reload > 1) {
						p.sendMessage(Main.main.pr + "Du kriegst neue Schneeb�lle in " + reload + " Sekunden!");

					}
					if (reload == 1) {
						p.sendMessage(Main.main.pr + "Du kriegst neue Schneeb�lle in " + reload + " Sekunde!");
						Main.main.reloadsnowball.remove(p);
						Main.main.reloadsnowball.remove(p);
						Main.main.reloadsnowball.remove(p);
						Main.main.reloadsnowball.remove(p);
						Main.main.reloadsnowball.remove(p);
						Main.main.reloadsnowball.remove(p);
						Main.main.reloadsnowball.remove(p);
						Main.main.reloadsnowball.remove(p);
					}
					if (reload == 0) {
						Main.main.reloadsnowball.remove(p);
						if (Main.main.kitstandard.contains(p)) {
							km.giveStandardKit(p);
							stopReload(p);
						} else if (Main.main.kitmagneto.contains(p)) {
							km.giveMagnetoKit(p);
							stopReload(p);
						} else if (Main.main.kitshotgun.contains(p)) {
							km.giveShutgunKit(p);
							stopReload(p);
						} else {
							p.getInventory().addItem(new ItemStack(Material.SNOW_BALL, 64));
							stopReload(p);
						}
					}
					reload--;
				}
			}
		}, 20, 20);
		return reloadsnowball;
	}

	@Deprecated
	public void stopReload(Player p) {
		Main.main.reload.get(p).cancel();
		Main.main.reload.get(p).cancel();
		Main.main.reload.get(p).cancel();
		reloadsnowball.cancel();
		Main.main.reloadsnowball.remove(p);
		Main.main.reloadsnowball.remove(p);
		Main.main.reloadsnowball.remove(p);

	}

	public void endGame() {
		Bukkit.getScheduler().cancelAllTasks();
		for (Player all : Bukkit.getOnlinePlayers()) {
			utils.clearPlayer(all);
			all.getWorld().playSound(all.getLocation(), Sound.NOTE_PLING, 1, 3);
			all.teleport(lm.getLocation("lobby"));
			CraftPlayer p = (CraftPlayer) all;
			IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + "�4�lSpiel vorbei!" + "\"}");
			PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppoc);
			Main.main.reloadsnowball.remove(p);
			Main.main.teamblau.remove(p);
			Main.main.teamrot.remove(p);
			Main.main.respawning.remove(p);
		}

		if (Main.main.teamredpoints >= 1) {
			Bukkit.broadcastMessage(
					Main.main.pr + ChatColor.RED + "Team Rot " + ChatColor.GRAY + " hat das Spiel gewonnen!");
		} else if (Main.main.teambluepoints >= 1) {
			Bukkit.broadcastMessage(
					Main.main.pr + ChatColor.BLUE + "Team Blau " + ChatColor.GRAY + " hat das Spiel gewonnen!");
		} else {
			Bukkit.broadcastMessage(Main.main.pr + "Das Spiel ist unentschieden ausgegangen!");
		}
		startRestartCD();
	}
}