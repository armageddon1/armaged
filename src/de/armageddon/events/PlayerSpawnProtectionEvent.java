package de.armageddon.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.scheduler.BukkitTask;

import de.armageddon.Main;

public class PlayerSpawnProtectionEvent extends PlayerEvent{

	private static final HandlerList handlers = new HandlerList();
	private int sekunden;
	private Player player;
	private BukkitTask protection;
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	public PlayerSpawnProtectionEvent(Player player, int sekunden) {
		super(player);
		this.player = player;
		this.sekunden = sekunden;
		protect();
	}
	
	public void protect() {
		Main.main.protection.add(player);
		protection = Bukkit.getScheduler().runTaskTimerAsynchronously(Main.main, new Runnable() {

			@Override
			public void run() {
            	if (sekunden > 1) {
					player.sendMessage(Main.main.pr + "Du hast Spawn Protection f�r " + sekunden + " Sekunden!");

				}
				if (sekunden == 1) {
					player.sendMessage(Main.main.pr + "Du hast Spawn Protection f�r " + sekunden + " Sekunde!");
				}
				if (sekunden == 0) {
					player.sendMessage(Main.main.pr + "Du hast keine Spawn Protection mehr!");
                    endSpawnProtection();
                }
                sekunden--;
			}
			
			
		}, 20, 20);
	}
	
	public void endSpawnProtection() {
		protection.cancel();
		Main.main.protection.remove(player);
	}
}
