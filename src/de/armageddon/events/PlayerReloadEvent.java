package de.armageddon.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.scheduler.BukkitTask;

import de.armageddon.Main;
import de.armageddon.Manager.KitManager;

public class PlayerReloadEvent extends PlayerEvent{
	
KitManager km = new KitManager();
	private static final HandlerList handlers = new HandlerList();

	
	public HandlerList getHandlers() {
		return handlers;
	}
	
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    private int sekunden;
    private Player player;
    private BukkitTask reload;
    
    public PlayerReloadEvent(Player player, int sekunden) {
    	super(player);
    	this.player = player;
    	this.sekunden = sekunden;
    	player.getInventory().clear();
    	startReload();
    }
    
    public void startReload() {
    	reload = Bukkit.getScheduler().runTaskTimerAsynchronously(Main.main, new Runnable() {
            public void run() {
            	if (sekunden > 1) {
					player.sendMessage(Main.main.pr + "Du kriegst neue Schneebälle in " + sekunden + " Sekunden!");

				}
				if (sekunden == 1) {
					player.sendMessage(Main.main.pr + "Du kriegst neue Schneebälle in " + sekunden + " Sekunde!");
				}
				if (sekunden == 0) {
                    endReload(reload);
                }
                sekunden--;
            }
        }, 20L, 20L);
    }

	public void endReload( BukkitTask task) {
		task.cancel();
		Main.main.reloadsnowball.remove(player);
		km.giveKit(player);
	}

}
