package de.armageddon.events;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.scheduler.BukkitTask;

import de.armageddon.Main;
import de.armageddon.Manager.KitManager;
import de.armageddon.Manager.TeamIdentifier;

public class PlayerRespawnEvent extends PlayerEvent {

	private static final HandlerList handlers = new HandlerList();

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	private int sekunden;
	int low = 0;
	int high = 3;
	Random rnd = new Random();
	private Player player;
	private BukkitTask respawn;
	TeamIdentifier ti = new TeamIdentifier();
	KitManager km = new KitManager();

	public PlayerRespawnEvent(Player player, int sekunden) {
		super(player);
		this.player = player;
		this.sekunden = sekunden;
		startRespawn();

	}
	
	public void startRespawn() {
		respawn = Bukkit.getScheduler().runTaskTimerAsynchronously(Main.main, new Runnable() {

			@Override
			public void run() {
					if (sekunden > 1) {
						player.sendMessage(Main.main.pr + "Du respawnst in " + sekunden + " Sekunden!");
					}
					
					if (sekunden <= 3) {
						player.getWorld().playSound(player.getLocation(), Sound.NOTE_PLING, 1, 0);
					}
					if (sekunden == 1) {
						player.sendMessage(Main.main.pr + "Du respawnst in " + sekunden + " Sekunde!");
					}
				if (sekunden == 0) {
					Main.main.respawning.remove(player);
					player.sendMessage(Main.main.pr + Main.main.teleportrespawnsekunde.replace("{remain}", "1"));
					if (Main.main.teamblau.contains(player)) {
						int spawn0 = rnd.nextInt(high - low) + low;
						player.setExp(0);
						player.setLevel(0);
						player.teleport(Main.main.spawns0.get(spawn0));
						player.getWorld().playSound(player.getLocation(), Sound.NOTE_PLING, 1, 3);
						Main.main.respawn10.remove(player);
						player.sendMessage(Main.main.pr + Main.main.teleportrespawn);
						Bukkit.getPluginManager().callEvent(new PlayerSpawnProtectionEvent(player, 5));
						Main.main.utils.clearPlayer(player);
						ti.giveChestplate(player);
						km.giveKit(player);
					} else if (Main.main.teamrot.contains(player)) {
						int spawn1 = rnd.nextInt(high - low) + low;
						player.setExp(0);
						player.setLevel(0);
						player.teleport(Main.main.spawns1.get(spawn1));
						player.getWorld().playSound(player.getLocation(), Sound.NOTE_PLING, 1, 3);
						Main.main.respawn10.remove(player);
						player.sendMessage(Main.main.pr + Main.main.teleportrespawn);
						Bukkit.getPluginManager().callEvent(new PlayerSpawnProtectionEvent(player, 5));
						Main.main.utils.clearPlayer(player);
						ti.giveChestplate(player);
						km.giveKit(player);
					}
					endRespawn();
				}
				sekunden--;
			}

		}, 20, 20);
	}

	public void endRespawn() {
		respawn.cancel();
		Main.main.respawn1.remove(player);
	}

	
	public Player getPl() {
		return player;
	}
	
	public int getSek() {
		return sekunden;
	}

}
