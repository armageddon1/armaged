package de.armageddon;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import de.armageddon.CMD.Cmd_debug;
import de.armageddon.CMD.Cmd_feedback;
import de.armageddon.CMD.Cmd_override;
import de.armageddon.CMD.Cmd_setlobby;
import de.armageddon.CMD.Cmd_setrespawn;
import de.armageddon.CMD.Cmd_setspawn;
import de.armageddon.CMD.Cmd_startgame;
import de.armageddon.Listener.FlagListener;
import de.armageddon.Listener.KickListener;
import de.armageddon.Listener.OnlineListerner;
import de.armageddon.Listener.SnowballListener;
import de.armageddon.Listener.StuffListener;
import de.armageddon.Manager.KitManager;
import de.armageddon.Manager.LocationManager;
import de.armageddon.Manager.MySQLManager;
import de.armageddon.countdowns.Countdown;
import de.armageddon.events.PlayerRespawnEvent;

public class Main extends JavaPlugin implements Listener {
	
	public ArrayList<Player> lobby = new ArrayList<Player>();
	public ArrayList<Player> teamblau = new ArrayList<Player>();
	public ArrayList<Player> teamrot = new ArrayList<Player>();
	public ArrayList<Player> respawn1 = new ArrayList<Player>();
	public ArrayList<Player> respawn2 = new ArrayList<Player>();
	public ArrayList<Player> respawn3 = new ArrayList<Player>();
	public ArrayList<Player> respawn4 = new ArrayList<Player>();
	public ArrayList<Player> respawn5 = new ArrayList<Player>();
	public ArrayList<Player> respawn6 = new ArrayList<Player>();
	public ArrayList<Player> respawn7 = new ArrayList<Player>();
	public ArrayList<Player> respawn8 = new ArrayList<Player>();
	public ArrayList<Player> respawn9 = new ArrayList<Player>();
	public ArrayList<Player> respawn10 = new ArrayList<Player>();
	public ArrayList<Player> respawning = new ArrayList<Player>();
	public ArrayList<Location> spawns0 = new ArrayList<Location>();
	public ArrayList<Location> spawns1 = new ArrayList<Location>();
	public ArrayList<Player> protection;
	public ArrayList<Player> reloadsnowball;
	public ArrayList<Player> flagge = new ArrayList<Player>();
	public ArrayList<Player> kitstandard = new ArrayList<Player>();
	public ArrayList<Player> kitmagneto = new ArrayList<Player>();
	public ArrayList<Player> kitshotgun = new ArrayList<Player>();
	public ArrayList<Player> kitselected = new ArrayList<Player>();
	public ArrayList<Player> magnetosingle = new ArrayList<Player>();
	public ConcurrentHashMap<Player, BukkitTask> cm;
	public ConcurrentHashMap<Player, BukkitTask> reload;
	
	public Gamestate state;
	public static Main main;
	public String pr;
	public String joinmessage;
	public String leavemessage;
	public String nopermission;
	public String winmsg;
	public String killmsgself;
	public String killmsg;
	public String setlobby;
	public String setspawn0;
	public String setspawn1;
	public String nurspieler;
	public String errorspawn;
	public String remaining;
	public String remainingmore;
	public String countdownstopped;
	public String teleportrespawnsekunden;
	public Utils utils;
	public String teleportrespawnsekunde;
	public String teleportrespawn;
	public String setrespawn;
	public String scoreboardtitle;
	public String host;
	public String port;
	public String user;
	public String db;
	public String pw;
	public int teamredpoints = 0;
	public int teambluepoints = 0;
	public int seklobby;
	public int sekrestart;
	public int respawntime = 10;
	public int sekingame;
	public int min;
	public int max;
	public LocationManager lm;
	public Countdown cd;
	public boolean cntstarted = false;
	public boolean lobbystarted = false;
	public String flagred = "Niemand";
	public String flagblue = "Niemand";
	public String richtungrot;
	public String richtungblau;
	public boolean btCreated = false;
	public boolean rtCreated = false;
	public boolean connectedtodb;
	
	public MySQLManager sql;
	public Connection c;

	
	File cfg = new File(this.getDataFolder(), "config.yml");
	FileConfiguration cfgf = YamlConfiguration.loadConfiguration(cfg);
	public void onEnable() {
		protection = new ArrayList<Player>();
		reloadsnowball = new ArrayList<Player>();
		cm = new ConcurrentHashMap<>();
		cfgf.options().copyDefaults(true);
		cfgf.addDefault("maxplayers", 10);
		cfgf.addDefault("minplayers", 2);
		cfgf.addDefault("seklobby", 60);
		cfgf.addDefault("sekingame", 600);
		cfgf.addDefault("sekrestart", 15);
		cfgf.addDefault("mysqlhost", "localhost");
		cfgf.addDefault("mysqlport", "3306");
		cfgf.addDefault("mysqldb", "paintball");
		cfgf.addDefault("mysqluser", "jendrik");
		cfgf.addDefault("mysqlpw", "gSDnNJvc");
		utils = new Utils();
		saveCfg();
		min = cfgf.getInt("minplayers");
		max = cfgf.getInt("maxplayers");
		sekingame = cfgf.getInt("sekingame");
		seklobby = cfgf.getInt("seklobby");
		sekrestart = cfgf.getInt("sekrestart");
		host = cfgf.getString("mysqlhost");
		port = cfgf.getString("mysqlport");
		db = cfgf.getString("mysqldb");
		user = cfgf.getString("mysqluser");
		pw = cfgf.getString("mysqlpw");
		System.out.println("HOST" + host);
		System.out.println("PORT" + port);
		System.out.println("USR" + user);
		System.out.println("PW" + pw);
		main = this;
		this.getCommand("setlobby").setExecutor(new Cmd_setlobby());
		this.getCommand("setspawn").setExecutor(new Cmd_setspawn());
		this.getCommand("setrespawn").setExecutor(new Cmd_setrespawn());
		this.getCommand("start").setExecutor(new Cmd_startgame());
		this.getCommand("feedback").setExecutor(new Cmd_feedback());
		this.getCommand("debug").setExecutor(new Cmd_debug());
		this.getCommand("override").setExecutor(new Cmd_override());
		lm = new LocationManager();
		cd = new Countdown();
		//cd.startRespawnCD();
		//cd.startReloadSnowball();
		
		lm.saveCfg();
		
		
		lm.saveMsg();
		lm.register();
		lm.readMsg();
		lm.delFile();
		
		try {
			sql = new MySQLManager(host, port, db, user, pw);
			c = sql.openConnection();
			Statement statement = c.createStatement();
			statement.executeUpdate(
					"CREATE TABLE IF NOT EXISTS `feedback` (UUID text, Nachricht text, datum text);");
			main.connectedtodb = true;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("[PaintBall] Kann kein Verbindung zur Datenbank herstellen, Feedback Kommando ist deaktiviert!");
			connectedtodb = false;
			e.printStackTrace();
		}
		
		this.getServer().getPluginManager().registerEvents(new OnlineListerner(), this);
		this.getServer().getPluginManager().registerEvents(new SnowballListener(), this);
		this.getServer().getPluginManager().registerEvents(new StuffListener(), this);
		this.getServer().getPluginManager().registerEvents(new FlagListener(), this);
		this.getServer().getPluginManager().registerEvents(new KickListener(), this);
		this.getServer().getPluginManager().registerEvents(new KitManager(), this);
		state = Gamestate.LOBBY;
		
	}
	
	@Override
	public boolean onCommand(CommandSender sn, Command cmd, String lb, String[] args) {
		return false;
	}

	public void saveCfg() {
		try {
			cfgf.save(cfg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}