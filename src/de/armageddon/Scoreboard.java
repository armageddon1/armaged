package de.armageddon;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;


import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.IChatBaseComponent;
import net.minecraft.server.v1_8_R1.PacketPlayOutChat;

public class Scoreboard {

	String title = Main.main.scoreboardtitle;
	org.bukkit.scoreboard.Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
	Objective ob = sb.registerNewObjective("score", "score");
	Score time;
	public int sched;
	int scored = 0;

	public void updateScoreboard(Player player) {
		int timeleft = Main.main.sekingame;
		// int zeit;
		////
		final int MINUTES_IN_AN_HOUR = 60;
		final int SECONDS_IN_A_MINUTE = 60;

		int seconds = timeleft % SECONDS_IN_A_MINUTE;
		int totalMinutes = timeleft / SECONDS_IN_A_MINUTE;
		int minutes = totalMinutes % MINUTES_IN_AN_HOUR;
		IChatBaseComponent cbc;
		////
		// Score time;
		if (timeleft > 60) {
			if (seconds < 10) {
		         cbc = ChatSerializer.a("{\"text\": \"" + "�e�lVerbleibende Zeit: " + minutes + ":0" + seconds + "    �4�lRote Flagge: " + Main.main.flagred + "" + "    �1�lBlaue Flagge: " + Main.main.flagblue + "\"}");
			} else {
				 cbc = ChatSerializer.a("{\"text\": \"" + "�e�lVerbleibende Zeit: " + minutes + ":" + seconds + "    �4�lRote Flagge: " + Main.main.flagred + "" + "    �1�lBlaue Flagge: " + Main.main.flagblue + "\"}");
			}
		} else {
			if (timeleft == 1) {
				 cbc = ChatSerializer.a("{\"text\": \"" + "�e�lVerbleibende Zeit: " + timeleft + " Sekunde"  + "    �4�lRote Flagge: " + Main.main.flagred + "" + "    �1�lBlaue Flagge: " + Main.main.flagblue + "\"}");
			} else if (timeleft > 1){
				 cbc = ChatSerializer.a("{\"text\": \"" + "�e�lVerbleibende Zeit: " + timeleft + " Sekunden"  + "    �4�lRote Flagge: " + Main.main.flagred + "" + "    �1�lBlaue Flagge: " + Main.main.flagblue + "\"}");
			} else {
				cbc = ChatSerializer.a("{\"text\": \"" + "           " + "\"}");
			}
		}
        CraftPlayer p = (CraftPlayer) player;

        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppoc);

	}

	public void createScoreboard(Player player) {
		sched = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.main, new Runnable() {

			@Override
			public void run() {
				updateScoreboard(player);
			}
			
			
		}
				
				
				
				, 20, 20);
	}

}
