package de.armageddon.Listener;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import de.armageddon.Gamestate;
import de.armageddon.Main;
import de.armageddon.Manager.BannerSetter;
import de.armageddon.Manager.HitKitManager;
import de.armageddon.Manager.LocationManager;
import de.armageddon.countdowns.Countdown;
import de.armageddon.events.PlayerRespawnEvent;
import de.armageddon.events.PlayerSpawnProtectionEvent;
import net.md_5.bungee.api.ChatColor;

public class SnowballListener implements Listener {
	Countdown cnt = new Countdown();
	LocationManager lm = new LocationManager();
	BannerSetter bs = new BannerSetter();
	public int timecd;
	HitKitManager hk = new HitKitManager();

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		e.setCancelled(true);
		if (Main.main.state == Gamestate.INGAME) {
			Projectile sb = (Projectile) e.getDamager();
			if (sb.getShooter() instanceof Player) {
				if (sb.getType().equals(EntityType.SNOWBALL)) {
					if (e.getEntity() instanceof Player) {
						Player killer = (Player) sb.getShooter();
						Player victim = (Player) e.getEntity();
						if (Main.main.teamblau.contains(victim)) {
							if (Main.main.teamblau.contains(killer)) {
							} else if (Main.main.teamrot.contains(killer)) {
								if (Main.main.protection.contains(victim)) {
									if (hk.checkKit(victim) == true) {
										Bukkit.broadcastMessage("zwei");
									} else {
										Bukkit.broadcastMessage("eins");
										killer.sendMessage(Main.main.pr
												+ "Die Attacke wurde von der R�stung des Gegners abgefangen!");
										Main.main.protection.remove(victim);
									}
								} else {
									respawn(killer, victim);
								}
							}
						} else if (Main.main.teamrot.contains(victim)) {
							if (Main.main.teamrot.contains(killer)) {

							} else if (Main.main.teamblau.contains(killer)) {
								if (Main.main.protection.contains(victim)) {
								} else {
									if (hk.checkKit(victim) == true) {
										respawn(killer, victim);
									} else {
										killer.sendMessage(Main.main.pr
												+ "Die Attacke wurde von der R�stung des Gegners abgefangen!");
										Main.main.protection.remove(victim);
									}
								}
							}
						}

					}
				}
			}
		}

	}

	public void respawn(Player killer, Player victim) {
		if (Main.main.teamrot.contains(victim)) {
			String vic = victim.getName();
			killer.sendMessage(Main.main.pr + Main.main.killmsg.replace("{victim}", vic));
			victim.sendMessage(Main.main.pr + Main.main.killmsgself.replace("{killer}", killer.getName()));
			victim.getInventory().clear();
			lm.addKill(killer);
			lm.addDeath(victim);
			lm.savePlayers();
			Main.main.reloadConfig();
			if (Main.main.flagge.contains(victim)) {
				Main.main.flagge.remove(victim);
				bs.blauesBanner();
				for (Player all : Bukkit.getOnlinePlayers()) {
					all.getWorld().playSound(all.getLocation(), Sound.NOTE_PLING, 1, 3);
				}
				Bukkit.broadcastMessage(Main.main.pr + victim.getName() + " hat die Fahne von Team " + ChatColor.BLUE
						+ " Blau" + ChatColor.GRAY + " verloren!");
				Main.main.flagblue = "Niemand";

			}
			victim.teleport(lm.getLocation("respawn"));
			Main.main.respawning.add(victim);
			victim.getInventory().clear();
			Bukkit.getPluginManager().callEvent(new PlayerRespawnEvent(victim, 10));

			// clear inventory
		} else if (Main.main.teamblau.contains(victim)) {
			String vic = victim.getName();
			killer.sendMessage(Main.main.pr + Main.main.killmsg.replace("{victim}", vic));
			victim.sendMessage(Main.main.pr + Main.main.killmsgself.replace("{killer}", killer.getName()));
			Main.main.utils.clearPlayer(victim);
			victim.getInventory().clear();
			lm.addKill(killer);
			lm.addDeath(victim);
			lm.savePlayers();
			Main.main.reloadConfig();
			Main.main.getServer().getScheduler().scheduleSyncDelayedTask(Main.main, new Runnable() {
				public void run() {
				}
			}, 10);
			if (Main.main.flagge.contains(victim)) {
				Main.main.flagge.remove(victim);
				bs.rotesBanner();
				for (Player all : Bukkit.getOnlinePlayers()) {
					all.getWorld().playSound(all.getLocation(), Sound.NOTE_PLING, 1, 3);
				}
				Bukkit.broadcastMessage(Main.main.pr + victim.getName() + " hat die Fahne von Team " + ChatColor.RED
						+ " Rot" + ChatColor.GRAY + " verloren!");
				Main.main.flagred = "Niemand";
			}
			victim.teleport(lm.getLocation("respawn"));
			Main.main.respawning.add(victim);
			victim.getInventory().clear();
			Bukkit.getPluginManager().callEvent(new PlayerRespawnEvent(victim, 10));
			// clear inventory
		}
	}
}