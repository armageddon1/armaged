package de.armageddon.Listener;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.armageddon.Gamestate;
import de.armageddon.Main;
import net.md_5.bungee.api.ChatColor;

public class KickListener implements Listener {

	ItemStack kickitem = new ItemStack(Material.SLIME_BALL, 1);
	ItemMeta kickitem_meta = kickitem.getItemMeta();

	public void giveKickItem(Player p) {
		kickitem_meta.setDisplayName(ChatColor.RED + "Kick mich");
		kickitem_meta.setLore(
				Arrays.asList(ChatColor.RED + "Wenn du einen Rechtsklick auf dieses Item machst wirst du gekickt!"));
		kickitem.setItemMeta(kickitem_meta);
		p.getInventory().addItem(kickitem);
	}

	@EventHandler
	public void onRightClick(PlayerInteractEvent e) {
		if (Main.main.state == Gamestate.RESTART) {
			Player p = e.getPlayer();
			if (p.getItemInHand().getType() == Material.SLIME_BALL) {
				p.kickPlayer(ChatColor.RED + "Voila");
			}

		}

	}

}
