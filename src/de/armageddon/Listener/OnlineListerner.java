package de.armageddon.Listener;

import java.util.Collection;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.armageddon.Gamestate;
import de.armageddon.Main;
import de.armageddon.Manager.Balancer;
import de.armageddon.Manager.LocationManager;
import de.armageddon.Manager.StartGame;
import de.armageddon.Manager.StartGameNew;
import de.armageddon.countdowns.Countdown;

public class OnlineListerner implements Listener {

	Countdown cnt = new Countdown();
	LocationManager lm = new LocationManager();
	Random rn = new Random();
	Balancer b = new Balancer();
	StartGame game = new StartGame();
	

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
        ItemStack item = new ItemStack(Material.CHEST, 1);
        ItemMeta met = item.getItemMeta();
        met.setDisplayName("�a�lKits");
        item.setItemMeta(met);
		if (Main.main.state == Gamestate.LOBBY) {
			if (!Main.main.lobby.contains(p)) {
				Main.main.lobby.add(p);
			}
			Main.main.utils.clearPlayer(p);
			p.teleport(Main.main.lm.getLocation("lobby"));
	        p.getInventory().setItem(0, item);
			String name = Main.main.joinmessage.replace("{player}", "" + p.getName());
			e.setJoinMessage(Main.main.pr + name);
			if (Main.main.lobby.size() >= Main.main.min) {
				if (Main.main.cntstarted == true) {
				} else {
					new StartGameNew(60);
				}
			} else {
				int remain = Main.main.min - Main.main.lobby.size();
				String msgmore = Main.main.remainingmore.replace("{remain}", "" + remain);
				String msgone = Main.main.remaining;
				if (remain == 1) {
					Bukkit.broadcastMessage(Main.main.pr + msgone);
				} else if (remain == 0){
					
				} else if (remain > 1){
					Bukkit.broadcastMessage(Main.main.pr + msgmore);
				}
			}
		}
	}

	@EventHandler
	public void onLogin(PlayerLoginEvent e) {
		if (Main.main.state != Gamestate.LOBBY) {
			e.disallow(Result.KICK_FULL, "�cDas Spiel hat schon begonnen!");
			return;
		} else if (Main.main.lobby.size() >= Main.main.max) {
			e.disallow(Result.KICK_FULL, "�cDas Spiel ist voll!");
		}
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		if (Main.main.state == Gamestate.LOBBY) {
			Main.main.lobby.remove(p);
			String name = Main.main.leavemessage.replace("{player}", "" + p.getName());
			e.setQuitMessage(Main.main.pr + name);
		}
		if (Main.main.state == Gamestate.LOBBY) {
				int remain = Main.main.min - Main.main.lobby.size();
				String msgmore = Main.main.remainingmore.replace("{remain}", "" + remain);
				String msgone = Main.main.remaining;
				if (remain == 1) {
					Bukkit.broadcastMessage(Main.main.pr + Main.main.countdownstopped);
					for (Player all : Bukkit.getOnlinePlayers()) {
						all.setLevel(0);
						all.setExp(0);
					}
					game.stopCountdown();
					Bukkit.broadcastMessage(Main.main.pr + msgone);
				} else if (remain == 0){
				} else if (remain > 1){
					Bukkit.broadcastMessage(Main.main.pr + msgmore);
				}
		}
		if (Main.main.state == Gamestate.INGAME) {
			if (Main.main.teamblau.contains(p)) {
				Main.main.teamblau.remove(p);
				b.balance(p, "blau");
			} else if (Main.main.teamrot.contains(p)) {
				Main.main.teamrot.remove(p);
				b.balance(p, "rot");
			}
		}
		if (Main.main.state == Gamestate.RESTART) {
			Collection<? extends Player> playerc = Bukkit.getOnlinePlayers();
			if (playerc.size() == 0) {
				Main.main.getServer().shutdown();
			}
		}
	}

}