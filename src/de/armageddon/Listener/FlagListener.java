package de.armageddon.Listener;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import de.armageddon.Gamestate;
import de.armageddon.Main;
import de.armageddon.Manager.LocationManager;
import de.armageddon.countdowns.Countdown;
import net.md_5.bungee.api.ChatColor;

public class FlagListener implements Listener {

	LocationManager lm = new LocationManager();
	ItemStack fl = new ItemStack(Material.STANDING_BANNER, 1);
	Countdown cnt = new Countdown();
	ItemStack fr = new ItemStack(Material.BANNER, 1);
	BannerMeta fr_meta = (BannerMeta) fr.getItemMeta();
	ItemStack fb = new ItemStack(Material.BANNER, 1);
	BannerMeta fb_meta = (BannerMeta) fb.getItemMeta();

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		if (Main.main.state == Gamestate.INGAME) {
			Player p = e.getPlayer();
			Location loc = lm.getSpawn(1);
			Location loc0 = lm.getSpawn(0);
			Location lo = p.getLocation();
			if (Main.main.teamblau.contains(p)) {
				if (loc.getBlockX() == lo.getBlockX() && loc.getBlockY() == lo.getBlockY()
						&& loc.getBlockZ() == lo.getBlockZ()) {
					if (Main.main.flagge.contains(p)) {

					} else {
						Main.main.flagge.add(p);
						lm.getSpawn(1).getBlock().setType(Material.AIR);
						fr_meta.setBaseColor(DyeColor.RED);
						fr.setItemMeta(fr_meta);
						p.getInventory().setHelmet(fr);
						for (Player all : Bukkit.getOnlinePlayers()) {
							all.getWorld().playSound(all.getLocation(), Sound.NOTE_PLING, 1, 1);
						}
						Bukkit.broadcastMessage(Main.main.pr + p.getName() + " hat die Flagge von Team " + ChatColor.RED
								+ "Rot " + ChatColor.GRAY + "erobert!");
						Main.main.flagred = p.getName();
					}
				} else if (loc0.getBlockX() == lo.getBlockX() && loc0.getBlockY() == lo.getBlockY()
						&& loc0.getBlockZ() == lo.getBlockZ() && Main.main.flagge.contains(p)) {
					Bukkit.broadcastMessage(Main.main.pr + p.getName() + " hat seinem Team zum Gewinn verholfen");
					Bukkit.getScheduler().cancelTask(cnt.gamecd);
					Main.main.teambluepoints += 1;
					cnt.endGame();
				}
			} else if (Main.main.teamrot.contains(p)) {
				if (loc0.getBlockX() == lo.getBlockX() && loc0.getBlockY() == lo.getBlockY()
						&& loc0.getBlockZ() == lo.getBlockZ()) {
					if (Main.main.flagge.contains(p)) {

					} else {
						Main.main.flagge.add(p);
						lm.getSpawn(0).getBlock().setType(Material.AIR);
						fb_meta.setBaseColor(DyeColor.BLUE);
						fb.setItemMeta(fb_meta);
						p.getInventory().setHelmet(fb);
						for (Player all : Bukkit.getOnlinePlayers()) {
							all.getWorld().playSound(all.getLocation(), Sound.NOTE_PLING, 1, 1);
						}
						Bukkit.broadcastMessage(Main.main.pr + p.getName() + " hat die Flagge von Team " + ChatColor.BLUE
								+ "Blau " + ChatColor.GRAY + "erobert!");
						Main.main.flagblue = p.getName();
					}
				} else if (loc.getBlockX() == lo.getBlockX() && loc.getBlockY() == lo.getBlockY()
						&& loc.getBlockZ() == lo.getBlockZ() && Main.main.flagge.contains(p)) {
					Bukkit.broadcastMessage(Main.main.pr + p.getName() + " hat seinem Team zum Gewinn verholfen");
					Bukkit.getScheduler().cancelTask(cnt.gamecd);
					Main.main.teamredpoints += 1;
					cnt.endGame();
				}
			}
		}
	}
}