package de.armageddon.Listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.armageddon.Gamestate;
import de.armageddon.Main;
import de.armageddon.Manager.ReloadSnowball;
import de.armageddon.countdowns.Countdown;
import de.armageddon.events.PlayerReloadEvent;
import net.md_5.bungee.api.ChatColor;

public class StuffListener implements Listener {

	Countdown cnt = new Countdown();
	Inventory inv;
	ReloadSnowball rl = new ReloadSnowball();

	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onPickup(PlayerPickupItemEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onBedEnter(PlayerBedEnterEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onBlockDamage(EntityDamageByBlockEvent e) {
		e.setCancelled(true);
	}


	@EventHandler
	public void onClick1(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction().equals(Action.LEFT_CLICK_AIR) | e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
			if (Main.main.state == Gamestate.INGAME) {
				if (Main.main.respawning.contains(p)) {
					p.sendMessage(Main.main.pr + "Du kannst w�hrend der Respawnzeit nicht reloaden!");
				} else {
					if (!Main.main.reloadsnowball.contains(p)) {
						Main.main.reloadsnowball.add(p);
						Bukkit.getPluginManager().callEvent(new PlayerReloadEvent(p, 5));
					} else {
						p.sendMessage(Main.main.pr + ChatColor.RED + "Du reloadest bereits!");
					}
				}
			} else if (Main.main.state == Gamestate.LOBBY) {
				if (Main.main.state == Gamestate.LOBBY) {
					if (e.getMaterial() == Material.CHEST
							&& e.getItem().getItemMeta().getDisplayName().equals("�a�lKits")) {
						createInv(p);
					}
				}
			}

		} else if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (Main.main.state == Gamestate.LOBBY) {
				if (e.getMaterial() == Material.CHEST
						&& e.getItem().getItemMeta().getDisplayName().equals("�a�lKits")) {
					createInv(p);
				}
			}
		}
	}

	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		event.setFoodLevel(20);
	}

	public void createInv(Player p) {
		inv = Bukkit.getServer().createInventory(null, 9, "�a�lKits");
		ItemStack item = new ItemStack(Material.SNOW_BALL, 1);
		ItemStack magneto = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		ItemMeta magneta_meta = magneto.getItemMeta();
		ItemMeta met = item.getItemMeta();
		met.setDisplayName("�fStandard");
		magneta_meta.setDisplayName("�3Protection");
		magneto.setItemMeta(magneta_meta);
		item.setItemMeta(met);
		inv.setItem(0, item);
		inv.setItem(1, magneto);
		p.openInventory(inv);
	}
}
