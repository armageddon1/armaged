package de.armageddon.Manager;

import java.util.Collections;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import de.armageddon.Main;
import de.armageddon.countdowns.Countdown;

public class StartGameNew {

	private int time;
	LocationManager lm = new LocationManager();
	KitManager km = new KitManager();
	BannerSetter bs = new BannerSetter();
	Countdown cnt = new Countdown();
	Random rnd = new Random();
	int low = 0;
	int high = 3;
	int gamestart;

	public StartGameNew(int time) {
		if (Main.main.cntstarted == true) {
			Bukkit.getScheduler().cancelTask(gamestart);
			this.time = time;
			startGame();
		} else {
			this.time = time;
			startGame();
		}
	}

	private void startGame() {
		Main.main.cntstarted = true;
		for (int i = 0; i < 3; i++) {
			Location ln = lm.getSpawns0(i);
			Main.main.spawns0.add(ln);
			Location lnn = lm.getSpawns1(i);
			Main.main.spawns1.add(lnn);
		}
		gamestart = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.main, new Runnable() {

			@Override
			public void run() {
				if (time >= 1) {
					if (time == 45 || time == 30 || time == 20 || time == 10 || time <= 5 && time >= 1) {
						if (time == 1) {
							Bukkit.broadcastMessage(Main.main.pr + "Das Spiel startet in �6" + time + " �8Sekunde");
							for (Player all : Bukkit.getOnlinePlayers()) {
								all.getWorld().playSound(all.getLocation(), Sound.NOTE_PLING, 1, 3);
							}
						} else if (time <= 5) {
							Bukkit.broadcastMessage(Main.main.pr + "Das Spiel startet in �6" + time + " �8Sekunden");
							for (Player all : Bukkit.getOnlinePlayers()) {
								all.getWorld().playSound(all.getLocation(), Sound.NOTE_PLING, 1, 0);
							}
						} else {
							Bukkit.broadcastMessage(Main.main.pr + "Das Spiel startet in �6" + time + " �8Sekunden");
						}

					}
				} else if (time == 0) {
					Collections.shuffle(Main.main.lobby);
					int Groe�elobby = Main.main.lobby.size(); // 3
					if (Groe�elobby % 2 == 0) {
						for (int i = 0; i < Groe�elobby / 2; i++) {
							Main.main.teamblau.add(Main.main.lobby.get(i));
							Main.main.lobby.remove(i);
						}
						int Groe�e2lobby = Main.main.lobby.size();
						for (int i = 0; i < Groe�e2lobby; i++) {
							Main.main.teamrot.add(Main.main.lobby.get(i));
							Main.main.lobby.remove(i);
						}
					} else if (Groe�elobby % 2 == 1) {
						for (int i = 0; i < Groe�elobby / 2; i++) {
							Main.main.teamrot.add(Main.main.lobby.get(i));
							Main.main.lobby.remove(i);
						}
						int Groe�e2lobby = Main.main.lobby.size();
						for (int i = 0; i < Groe�e2lobby; i++) {
							Main.main.teamblau.add(Main.main.lobby.get(i));
							Main.main.lobby.remove(i);
						}
					}
					for (Player blau : Main.main.teamblau) {
						int spawn0 = rnd.nextInt(high - low) + low;
						blau.teleport(Main.main.spawns0.get(spawn0));
						Main.main.utils.clearPlayer(blau);
						km.giveKit(blau);
					}
					for (Player rot : Main.main.teamrot) {
						int spawn1 = rnd.nextInt(high - low) + low;
						rot.teleport(Main.main.spawns1.get(spawn1));
						Main.main.utils.clearPlayer(rot);
						km.giveKit(rot);
					}
					bs.rotesBanner();
					bs.blauesBanner();
					cnt.startGameCD();
					stopCountdown();
				}
				for (Player all : Bukkit.getOnlinePlayers()) {
					all.setLevel(time);
					all.setExp((float) (time * 0.01667));
				}
				time--;
			}

		}, 20, 20);

	}

	private void stopCountdown() {
		Bukkit.getScheduler().cancelTask(gamestart);
	}

}
