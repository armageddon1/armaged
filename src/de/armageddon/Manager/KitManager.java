package de.armageddon.Manager;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import de.armageddon.Gamestate;
import de.armageddon.Main;

public class KitManager implements Listener {

	public void giveStandardKit(Player p) {
		p.getInventory().addItem(new ItemStack(Material.SNOW_BALL, 64));
	}

	public void giveMagnetoKit(Player p) {
		Main.main.magnetosingle.add(p);
		p.getInventory().addItem(new ItemStack(Material.SNOW_BALL, 64));
	}

	public void giveShutgunKit(Player p) {

	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if (Main.main.state == Gamestate.LOBBY) {
			if (e.getCurrentItem().getType().toString().equals("SNOW_BALL")) {
				Main.main.kitstandard.add((Player) e.getWhoClicked());
				Main.main.kitselected.add((Player) e.getWhoClicked());
				Player p = (Player) e.getWhoClicked();
				p.closeInventory();
				p.sendMessage(Main.main.pr + "Du hast das Standard-Kit ausgewählt!");
			} else if (e.getCurrentItem().getType().toString().equals("LEATHER_CHESTPLATE")) {
				if (Main.main.kitselected.contains(e.getWhoClicked())) {
					Main.main.kitshotgun.remove(e.getWhoClicked());
					Main.main.kitstandard.remove(e.getWhoClicked());
				}
				Main.main.kitmagneto.add((Player) e.getWhoClicked());
				Main.main.kitselected.add((Player) e.getWhoClicked());
				Main.main.magnetosingle.add((Player) e.getWhoClicked());
				((Player)e.getWhoClicked()).sendMessage(Main.main.pr + "Du hast das Protection-Kit ausgewählt!");
				Player p = (Player) e.getWhoClicked();
				p.closeInventory();
			}
		} else if (Main.main.state == Gamestate.INGAME) {
			e.setCancelled(true);
		}
	}

	public void giveKit(Player p) {
		if (Main.main.kitmagneto.contains(p)) {
			giveMagnetoKit(p);
		} else if (Main.main.kitshotgun.contains(p)) {
			giveShutgunKit(p);
		} else {
			giveStandardKit(p);
		}
	}

}
