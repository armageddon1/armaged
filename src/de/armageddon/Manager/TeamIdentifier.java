package de.armageddon.Manager;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.ScoreboardManager;

import de.armageddon.Main;

public class TeamIdentifier {

	LocationManager lm = new LocationManager();
	ItemStack hr = new ItemStack(Material.LEATHER_HELMET, 1);
	LeatherArmorMeta hr_meta = (LeatherArmorMeta) hr.getItemMeta();
	ItemStack hb = new ItemStack(Material.LEATHER_HELMET, 1);
	LeatherArmorMeta hb_meta = (LeatherArmorMeta) hb.getItemMeta();
	ItemStack cpr = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
	LeatherArmorMeta cpr_meta = (LeatherArmorMeta) cpr.getItemMeta();
	ItemStack cpb = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
	LeatherArmorMeta cpb_meta = (LeatherArmorMeta) cpb.getItemMeta();
	ItemStack lb = new ItemStack(Material.LEATHER_LEGGINGS, 1);
	LeatherArmorMeta lb_meta = (LeatherArmorMeta) cpb.getItemMeta();
	ItemStack lr = new ItemStack(Material.LEATHER_LEGGINGS, 1);
	LeatherArmorMeta lr_meta = (LeatherArmorMeta) cpb.getItemMeta();
	ItemStack bb = new ItemStack(Material.LEATHER_BOOTS, 1);
	LeatherArmorMeta bb_meta = (LeatherArmorMeta) cpb.getItemMeta();
	ItemStack br = new ItemStack(Material.LEATHER_BOOTS, 1);
	LeatherArmorMeta br_meta = (LeatherArmorMeta) cpb.getItemMeta();

	public void giveChestplate(Player p) {
		if (Main.main.teamblau.contains(p)) {
			cpb_meta.setColor(Color.BLUE);
			hb_meta.setColor(Color.BLUE);
			hb.setItemMeta(hb_meta);
			cpb.setItemMeta(cpb_meta);
			lb_meta.setColor(Color.BLUE);
			lb.setItemMeta(lb_meta);
			bb_meta.setColor(Color.BLUE);
			bb.setItemMeta(bb_meta);
			p.getInventory().setChestplate(cpb);
			p.getInventory().setHelmet(hb);
			p.getInventory().setLeggings(lb);
			p.getInventory().setBoots(bb);
		} else if (Main.main.teamrot.contains(p)) {
			cpr_meta.setColor(Color.RED);
			hr_meta.setColor(Color.RED);
			hr.setItemMeta(hr_meta);
			cpr.setItemMeta(cpr_meta);
			lr_meta.setColor(Color.RED);
			lr.setItemMeta(lr_meta);
			br_meta.setColor(Color.RED);
			br.setItemMeta(br_meta);
			p.getInventory().setChestplate(cpr);
			p.getInventory().setHelmet(hr);
			p.getInventory().setLeggings(lr);
			p.getInventory().setBoots(br);
		}
	}

	@Deprecated
	public String gibTeam(Player p) {
		if (Main.main.teamblau.contains(p)) {
			return "Rot";
		} else {
			return "Blau";
		}
	}

	public void broadcastTeam(Player p) {
		if (Main.main.teamblau.contains(p)) {
			Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "title " + p.getName() + " title {text:'Du bist in Team " + ChatColor.BLUE + "Blau', color:white}");
		} else if (Main.main.teamrot.contains(p)) {
			Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "title " + p.getName() + " title {text:'Du bist in Team " + ChatColor.RED + "Rot', color:white}");
		}

	}

	public void setScoreboard(Player p) {
		ScoreboardManager sm = Bukkit.getScoreboardManager();
		org.bukkit.scoreboard.Scoreboard board = null;
		board = sm.getNewScoreboard();
		Objective obj = board.registerNewObjective("aza", "bzb");
		int kills = lm.getKills(p);
		int deaths = lm.getDeaths(p);

		obj.setDisplayName("PaintBall");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		obj.getScore("�e�lDeine Kills:").setScore(9);
		obj.getScore("�f" + kills + " ").setScore(8);
		obj.getScore(" ").setScore(7);
		obj.getScore("�e�lDeine Tode:").setScore(6);
		obj.getScore("�e�f" + deaths + " ").setScore(5);
		p.setScoreboard(board);
		
	}
}
