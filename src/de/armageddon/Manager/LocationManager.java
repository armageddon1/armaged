package de.armageddon.Manager;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.armageddon.Main;

public class LocationManager {

	public File file = new File("plugins/" + Main.main.getName(), "locs.yml");
	public File msgf = new File("plugins/" + Main.main.getName(), "messages.yml");
	public File redteam = new File("plugins/" + Main.main.getName(), "teamred.yml");
	public File blueteam = new File("plugins/" + Main.main.getName(), "teamblue.yml");
	public FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	public FileConfiguration msg = YamlConfiguration.loadConfiguration(msgf);
	public FileConfiguration rt = YamlConfiguration.loadConfiguration(redteam);
	public FileConfiguration bt = YamlConfiguration.loadConfiguration(blueteam);

	boolean btCreated = false;
	boolean rtCreated = false;

	public void register() {
		msg.options().copyDefaults(true);
		msg.addDefault("Prefix", "&7[&8PaintBall&7] ");
		msg.addDefault("joinmessage", "&a{player} ist beigetreten");
		msg.addDefault("leavemessage", "&4{player} hat das Spiel verlassen");
		msg.addDefault("nopermission", "&4Das darfst du nicht!");
		msg.addDefault("winmsg", "&aTeam {teamname} hat das Spiel gewonnen");
		msg.addDefault("killmsgself", "&aDu wurdest von {killer} gekillt");
		msg.addDefault("killmsg", "&aDu hast {victim} gekillt");
		msg.addDefault("setlobby", "&aDu hast die &6Lobby &agesetzt!");
		msg.addDefault("nurspieler", "&aDu musst ein Spieler sein.");
		msg.addDefault("setspawn0", "&aDu hast den Spawn f?r Blau &agesetzt!");
		msg.addDefault("setspawn1", "&aDu hast den Spawn f?r Rot &agesetzt!");
		msg.addDefault("errorspawn", "&4Nutze /setspawn <1>/<0> (<0|1|2>)");
		msg.addDefault("remainingmore", "&7Es fehlen noch &6{remain} &7Spieler");
		msg.addDefault("remaining", "&7Es fehlt noch &61 &7Spieler");
		msg.addDefault("countdownstopped", "&7Countdown wurde gestoppt, weil zu wenig Spieler vorhanden sind!");
		msg.addDefault("setrespawn", "&7Du hast den Respawn erfolgreich gesetzt!");
		msg.addDefault("teleportrespawnsekunden", "&7Es sind noch {remain} Sekunden bis du respawnst!");
		msg.addDefault("teleportrespawnsekunde", "&7Es ist noch 1 Sekunde bis du respawnst!");
		msg.addDefault("scoreboardtitle", "&6PaintBall");
		msg.addDefault("teleportrespawn", "&7Du wurdest erfolgreich respawnt!");
		msg.addDefault("richrot", "N");
		msg.addDefault("richblau", "S");
		saveMsg();

	}

	public void readMsg() {
		Main.main.pr = ChatColor.translateAlternateColorCodes('&', msg.getString("Prefix"));
		Main.main.joinmessage = ChatColor.translateAlternateColorCodes('&', msg.getString("joinmessage"));
		Main.main.leavemessage = ChatColor.translateAlternateColorCodes('&', msg.getString("leavemessage"));
		Main.main.nopermission = ChatColor.translateAlternateColorCodes('&', msg.getString("nopermission"));
		Main.main.winmsg = ChatColor.translateAlternateColorCodes('&', msg.getString("winmsg"));
		Main.main.killmsgself = ChatColor.translateAlternateColorCodes('&', msg.getString("killmsgself"));
		Main.main.killmsg = ChatColor.translateAlternateColorCodes('&', msg.getString("killmsg"));
		Main.main.setlobby = ChatColor.translateAlternateColorCodes('&', msg.getString("setlobby"));
		Main.main.nurspieler = ChatColor.translateAlternateColorCodes('&', msg.getString("nurspieler"));
		Main.main.setspawn0 = ChatColor.translateAlternateColorCodes('&', msg.getString("setspawn0"));
		Main.main.setspawn1 = ChatColor.translateAlternateColorCodes('&', msg.getString("setspawn1"));
		Main.main.errorspawn = ChatColor.translateAlternateColorCodes('&', msg.getString("errorspawn"));
		Main.main.remainingmore = ChatColor.translateAlternateColorCodes('&', msg.getString("remainingmore"));
		Main.main.remaining = ChatColor.translateAlternateColorCodes('&', msg.getString("remaining"));
		Main.main.countdownstopped = ChatColor.translateAlternateColorCodes('&', msg.getString("countdownstopped"));
		Main.main.setrespawn = ChatColor.translateAlternateColorCodes('&', msg.getString("setrespawn"));
		Main.main.teleportrespawn = ChatColor.translateAlternateColorCodes('&', msg.getString("teleportrespawn"));
		Main.main.teleportrespawnsekunde = ChatColor.translateAlternateColorCodes('&',
				msg.getString("teleportrespawnsekunde"));
		Main.main.scoreboardtitle = ChatColor.translateAlternateColorCodes('&', "scoreboardtitle");
		Main.main.teleportrespawnsekunden = ChatColor.translateAlternateColorCodes('&',
				msg.getString("teleportrespawnsekunden"));
		Main.main.richtungblau = msg.getString("richblau");
		Main.main.richtungrot = msg.getString("richrot");
	}

	public void addPlayers(Player p) {
		if (Main.main.teamblau.contains(p)) {
			bt.set(p.getName() + ".kills", 0);
			bt.set(p.getName() + ".deaths", 0);
		} else if (Main.main.teamrot.contains(p)) {
			rt.set(p.getName() + ".kills", 0);
			rt.set(p.getName() + ".deaths", 0);
		}
	}

	public void addKill(Player p) {
		if (Main.main.teamblau.contains(p)) {
			int oldKills = bt.getInt(p.getName() + ".kills");
			int newKills = oldKills + 1;
			bt.set(p.getName() + ".kills", newKills);
		} else if (Main.main.teamrot.contains(p)) {
			int oldKills = rt.getInt(p.getName() + ".kills");
			int newKills = oldKills + 1;
			rt.set(p.getName() + ".kills", newKills);

		}
		try {
			rt.save(redteam);
			bt.save(blueteam);
			rt = YamlConfiguration.loadConfiguration(redteam);
			bt = YamlConfiguration.loadConfiguration(blueteam);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void addDeath(Player p) {
		if (Main.main.teamblau.contains(p)) {
			int oldDeaths = bt.getInt(p.getName() + ".deaths");
			int newDeaths = oldDeaths + 1;
			bt.set(p.getName() + ".deaths", newDeaths);
		} else if (Main.main.teamrot.contains(p)) {
			int oldDeaths = rt.getInt(p.getName() + ".deaths");
			int newDeaths = oldDeaths + 1;
			rt.set(p.getName() + ".deaths", newDeaths);
		}
	}
	
	public void loadFile() {
		redteam = new File("plugins/" + Main.main.getName(), "teamred.yml");
		blueteam = new File("plugins/" + Main.main.getName(), "teamblue.yml");
		
		rt = YamlConfiguration.loadConfiguration(redteam);
		bt = YamlConfiguration.loadConfiguration(blueteam);
	}
	
	public void delFile() {
		redteam = new File("plugins/" + Main.main.getName(), "teamred.yml");
		blueteam = new File("plugins/" + Main.main.getName(), "teamblue.yml");
		
		rt = YamlConfiguration.loadConfiguration(redteam);
		bt = YamlConfiguration.loadConfiguration(blueteam);
		
		redteam.delete();
		blueteam.delete();
	}

	public int getKills(Player p) {
		loadFile();
		if (Main.main.teamblau.contains(p)) {
			int kills = bt.getInt(p.getName() + ".kills");
			return kills;
		} else if (Main.main.teamrot.contains(p)) {
			int kills = rt.getInt(p.getName() + ".kills");
			return kills;
		}
		return -1337;
	}

	public int getDeaths(Player p) {
		loadFile();
		if (Main.main.teamblau.contains(p)) {
			int deaths = bt.getInt(p.getName() + ".deaths");
			return deaths;
		} else if (Main.main.teamrot.contains(p)) {
			int deaths = rt.getInt(p.getName() + ".deaths");
			return deaths;
		}
		return -1337;
	}

	public void reloadPlayerStats() {
		Main.main.reloadConfig();
	}

	public void setLocation(String name, Location loc) {
		cfg.set(name + ".world", loc.getWorld().getName());
		cfg.set(name + ".X", loc.getX());
		cfg.set(name + ".Y", loc.getY());
		cfg.set(name + ".Z", loc.getZ());
		cfg.set(name + ".YAW", loc.getYaw());
		cfg.set(name + ".PITCH", loc.getPitch());
		saveCfg();
	}

	public Location getLocation(String name) {
		Location loc;
		try {
			World w = Bukkit.getWorld(cfg.getString(name + ".world"));
			double x = cfg.getDouble(name + ".X");
			double y = cfg.getDouble(name + ".Y");
			double z = cfg.getDouble(name + ".Z");
			loc = new Location(w, x, y, z);
			loc.setPitch(cfg.getInt(name + ".YAW"));
			loc.setPitch(cfg.getInt(name + ".PITCH"));

		} catch (Exception e) {
			loc = new Location(Bukkit.getWorlds().get(0), 0, 0, 0);
		}
		return loc;
	}

	public void setSpawn(int number, Location loc) {
		String name = "Spawn";
		cfg.set(name + "." + number + ".world", loc.getWorld().getName());
		cfg.set(name + "." + number + ".X", loc.getX());
		cfg.set(name + "." + number + ".Y", loc.getY());
		cfg.set(name + "." + number + ".Z", loc.getZ());
		cfg.set(name + "." + number + ".YAW", loc.getYaw());
		cfg.set(name + "." + number + ".PITCH", loc.getPitch());
		saveCfg();
	}

	public void setSpawns0(int number, Location loc) {
		String name = "Spawns0";
		cfg.set(name + "." + number + ".world", loc.getWorld().getName());
		cfg.set(name + "." + number + ".X", loc.getX());
		cfg.set(name + "." + number + ".Y", loc.getY());
		cfg.set(name + "." + number + ".Z", loc.getZ());
		cfg.set(name + "." + number + ".YAW", loc.getYaw());
		cfg.set(name + "." + number + ".PITCH", loc.getPitch());
		saveCfg();
	}

	public void setSpawns1(int number, Location loc) {
		String name = "Spawns1";
		cfg.set(name + "." + number + ".world", loc.getWorld().getName());
		cfg.set(name + "." + number + ".X", loc.getX());
		cfg.set(name + "." + number + ".Y", loc.getY());
		cfg.set(name + "." + number + ".Z", loc.getZ());
		cfg.set(name + "." + number + ".YAW", loc.getYaw());
		cfg.set(name + "." + number + ".PITCH", loc.getPitch());
		saveCfg();
	}

	public Location getSpawn(int number) {
		String name = "Spawn";
		World w = Main.main.getServer().getWorld(cfg.getString(name + "." + number + ".world"));
		double x = cfg.getDouble(name + "." + number + ".X");
		double y = cfg.getDouble(name + "." + number + ".Y");
		double z = cfg.getDouble(name + "." + number + ".Z");
		Location loc = new Location(w, x, y, z);
		loc.setPitch(cfg.getInt(name + "." + number + ".YAW"));
		loc.setPitch(cfg.getInt(name + "." + number + ".PITCH"));
		return loc;
	}

	public Location getSpawns0(int number) {
		String name = "Spawns0";
		World w = Main.main.getServer().getWorld(cfg.getString(name + "." + number + ".world"));
		double x = cfg.getDouble(name + "." + number + ".X");
		double y = cfg.getDouble(name + "." + number + ".Y");
		double z = cfg.getDouble(name + "." + number + ".Z");
		Location loc = new Location(w, x, y, z);
		loc.setPitch(cfg.getInt(name + "." + number + ".YAW"));
		loc.setPitch(cfg.getInt(name + "." + number + ".PITCH"));
		return loc;
	}

	public Location getSpawns1(int number) {
		String name = "Spawns1";
		World w = Main.main.getServer().getWorld(cfg.getString(name + "." + number + ".world"));
		double x = cfg.getDouble(name + "." + number + ".X");
		double y = cfg.getDouble(name + "." + number + ".Y");
		double z = cfg.getDouble(name + "." + number + ".Z");
		Location loc = new Location(w, x, y, z);
		loc.setPitch(cfg.getInt(name + "." + number + ".YAW"));
		loc.setPitch(cfg.getInt(name + "." + number + ".PITCH"));
		return loc;
	}

	public void saveCfg() {
		try {
			cfg.save(file);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void saveMsg() {
		try {
			msg.save(msgf);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void savePlayers() {
		try {
			rt.save(redteam);
			bt.save(blueteam);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
