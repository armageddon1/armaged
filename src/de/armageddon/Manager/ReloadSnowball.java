package de.armageddon.Manager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import de.armageddon.Main;

public class ReloadSnowball {
	 
    BukkitTask reload;
    int sekunden;
    
    KitManager km = new KitManager();
 
    public void rl(Player p) {
        sekunden = 5;
        p.getInventory().clear();
        Main.main.reloadsnowball.add(p);
        Main.main.reload.put(p, getTask(p));
 
    }
 
    public void endReload(Player p) {
        Main.main.reload.get(p).cancel();
        Main.main.reloadsnowball.remove(p);
		km.giveKit(p);
        
    }

    public BukkitTask getTask(Player p){
        reload = Bukkit.getScheduler().runTaskTimerAsynchronously(Main.main, new Runnable() {
            public void run() {
            	if (sekunden > 1) {
					p.sendMessage(Main.main.pr + "Du kriegst neue Schneebälle in " + sekunden + " Sekunden!");

				}
				if (sekunden == 1) {
					p.sendMessage(Main.main.pr + "Du kriegst neue Schneebälle in " + sekunden + " Sekunde!");
				}
				if (sekunden == 0) {
                    endReload(p);
                }
                sekunden--;
            }
        }, 20L, 20L);
      return reload;
    }
}