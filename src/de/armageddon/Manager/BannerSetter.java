package de.armageddon.Manager;

import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.material.Directional;

import de.armageddon.Main;

public class BannerSetter {

	LocationManager lm = new LocationManager();
	Block blockblau = lm.getSpawn(0).getBlock();
	Block blockrot = lm.getSpawn(1).getBlock();

	public void rotesBanner() {
		blockrot.setType(Material.STANDING_BANNER);
		Banner bannerrot = (Banner) blockrot.getState();
		bannerrot.setBaseColor(DyeColor.RED);
		try {
			if (Main.main.richtungrot.equals("N")) {
				((Directional) bannerrot.getData()).setFacingDirection(BlockFace.NORTH);
			} else if (Main.main.richtungrot.equals("O")) {
				((Directional) bannerrot.getData()).setFacingDirection(BlockFace.EAST);
			} else if (Main.main.richtungrot.equals("S")) {
				((Directional) bannerrot.getData()).setFacingDirection(BlockFace.SOUTH);
			} else if (Main.main.richtungrot.equals("W")) {
				((Directional) bannerrot.getData()).setFacingDirection(BlockFace.WEST);
			}
		} catch (Exception e) {
			((Directional) bannerrot.getData()).setFacingDirection(BlockFace.SOUTH);
		}
		bannerrot.update();
	}

	public void blauesBanner() {
		blockblau.setType(Material.STANDING_BANNER);
		Banner bannerblau = (Banner) blockblau.getState();
		bannerblau.setBaseColor(DyeColor.BLUE);
		try {
			if (Main.main.richtungrot.equals("N")) {
				((Directional) bannerblau.getData()).setFacingDirection(BlockFace.NORTH);
			} else if (Main.main.richtungrot.equals("O")) {
				((Directional) bannerblau.getData()).setFacingDirection(BlockFace.EAST);
			} else if (Main.main.richtungrot.equals("S")) {
				((Directional) bannerblau.getData()).setFacingDirection(BlockFace.SOUTH);
			} else if (Main.main.richtungrot.equals("W")) {
				((Directional) bannerblau.getData()).setFacingDirection(BlockFace.WEST);
			}
		} catch (Exception e) {
			((Directional) bannerblau.getData()).setFacingDirection(BlockFace.NORTH);
		}
		bannerblau.update();
	}

}