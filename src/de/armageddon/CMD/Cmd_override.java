package de.armageddon.CMD;

import java.util.Collections;
import java.util.Random;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.armageddon.Main;
import de.armageddon.Manager.BannerSetter;
import de.armageddon.Manager.KitManager;
import de.armageddon.countdowns.Countdown;

public class Cmd_override implements CommandExecutor{

	KitManager km = new KitManager();
	BannerSetter bs = new BannerSetter();
	Countdown cnt = new Countdown();
	Random rnd = new Random();
	int low = 0;
	int high = 3;
	
	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		Collections.shuffle(Main.main.lobby);
		int Groe�elobby = Main.main.lobby.size(); // 3
		if (Groe�elobby % 2 == 0) {
			for (int i = 0; i < Groe�elobby / 2; i++) {
				Main.main.teamblau.add(Main.main.lobby.get(i));
				Main.main.lobby.remove(i);
			}
			int Groe�e2lobby = Main.main.lobby.size();
			for (int i = 0; i < Groe�e2lobby; i++) {
				Main.main.teamrot.add(Main.main.lobby.get(i));
				Main.main.lobby.remove(i);
			}
		} else if (Groe�elobby % 2 == 1) {
			for (int i = 0; i < Groe�elobby / 2; i++) {
				Main.main.teamrot.add(Main.main.lobby.get(i));
				Main.main.lobby.remove(i);
			}
			int Groe�e2lobby = Main.main.lobby.size();
			for (int i = 0; i < Groe�e2lobby; i++) {
				Main.main.teamblau.add(Main.main.lobby.get(i));
				Main.main.lobby.remove(i);
			}
		}
		for (Player blau : Main.main.teamblau) {
			int spawn0 = rnd.nextInt(high - low) + low;
			blau.teleport(Main.main.spawns0.get(spawn0));
			Main.main.utils.clearPlayer(blau);
			km.giveKit(blau);
		}
		for (Player rot : Main.main.teamrot) {
			int spawn1 = rnd.nextInt(high - low) + low;
			rot.teleport(Main.main.spawns1.get(spawn1));
			Main.main.utils.clearPlayer(rot);
			km.giveKit(rot);
		}
		bs.rotesBanner();
		bs.blauesBanner();
		cnt.startGameCD();
		return true;
	}
	

}
