package de.armageddon.CMD;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.armageddon.Main;
import de.armageddon.Manager.StartGame;
import de.armageddon.Manager.StartGameNew;
import de.armageddon.countdowns.Countdown;

public class Cmd_startgame implements CommandExecutor {

	Countdown cn = new Countdown();
	StartGame game = new StartGame();

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if (!arg0.hasPermission("paintball.startgame")) {
			arg0.sendMessage(Main.main.pr + Main.main.nopermission);
			return false;
		} else {
			if (Main.main.cntstarted == true) {
				if (arg0 instanceof Player) {
					Player p = (Player) arg0;
					Bukkit.broadcastMessage(Main.main.pr + "Der Countdown wurde verk�rzt von " + p.getName());
					new StartGameNew(10);
					return true;
				} else {
					Player p = (Player) arg0;
					Bukkit.broadcastMessage(Main.main.pr + "Der Countdown wurde verk�rzt");
					new StartGameNew(10);
					return true;
				}
			} else {
				if (arg0 instanceof Player) {
					Player p = (Player) arg0;
					Bukkit.broadcastMessage(Main.main.pr + "Das Spiel wurde manuell gestartet von " + p.getName());
					Bukkit.broadcastMessage(Main.main.pr + "Das Spiel startet in " + cn.lobby + " Sekunden");
				} else {
					Bukkit.broadcastMessage(Main.main.pr + "Das Spiel wurde manuell gestartet");
					Bukkit.broadcastMessage(Main.main.pr + "Das Spiel startet in " + cn.lobby + " Sekunden");
				}
				new StartGameNew(60);
				return true;
			}
		}
	}

}
