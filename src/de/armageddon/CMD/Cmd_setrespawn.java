package de.armageddon.CMD;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.armageddon.Main;

public class Cmd_setrespawn implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("paintball.setspawn")) {
			sender.sendMessage(Main.main.pr + Main.main.nopermission);
			return false;
		} else {
			if (sender instanceof ConsoleCommandSender) {
				sender.sendMessage(Main.main.pr + Main.main.nurspieler);
			}
			Player p = (Player) sender;
			if (p.hasPermission("paintball.setrespawn")) {
				if (args.length != 0) {
					return true;
				}
				Main.main.lm.setLocation("respawn", p.getLocation());
				p.sendMessage(Main.main.pr + Main.main.setrespawn);
				return true;
			}
			p.sendMessage(Main.main.pr + Main.main.nopermission);

			return false;
		}
	}

}
