package de.armageddon.CMD;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.armageddon.Main;

public class Cmd_feedback implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		if (Main.main.connectedtodb == true) {
			if (args.length == 0) {
				sender.sendMessage(Main.main.pr + "Bitte schreibe eine Nachricht dazu! /feedback <nachricht>");
			} else {
				// String aus args erstellen
				String myString = "";
				for (int i = 0; i < args.length; i++) {
					myString = myString + args[i] + " ";
				}
				//Ich bin etwas paranoid was SQL Injection angeht sry :/
				myString = myString.replace("'", "_");
				myString = myString.replace("�", "_");
				myString = myString.replace(";", "_");
				myString = myString.replace("(", "_");
				myString = myString.replace(")", "_");
				myString = myString.replace(",", "_");
				Player p = (Player) sender;
				DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
				Calendar cal = Calendar.getInstance();
				String datum = dateFormat.format(cal.getTime());
				String UUID = p.getUniqueId().toString();
				String Nachricht = myString;
			try {
				Statement statement = Main.main.c.createStatement();
				statement.executeUpdate("INSERT INTO feedback (`UUID`, `Nachricht`, `datum`) VALUES ('" + UUID + "', '"+ Nachricht + "', '" + datum + "');");
				p.sendMessage(Main.main.pr + "Vielen Dank f�r dein Feedback!");
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
			}
			return false;
		} else {
			return false;
		}
	}

	public void broadcast() {
		Bukkit.broadcastMessage(Main.main.pr
				+ "Gef�llt dir etwas nicht an Paintball? Magst du etwas? Hast du einen Bug gefunden? Sag' uns doch bescheid mit /feedback!");
	}

}
