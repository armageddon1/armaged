package de.armageddon.CMD;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.armageddon.Main;

public class Cmd_setlobby implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
	if (!sender.hasPermission("paintball.setlobby")) {
		sender.sendMessage(Main.main.pr + Main.main.nopermission);
		return false;
	} else {
		if(sender instanceof Player) {
			Player p = (Player) sender;
			if(p.hasPermission("paintball.setlobby")) {
				if(args.length != 0) {
					return true;
				}
				Main.main.lm.setLocation("lobby", p.getLocation());
				p.sendMessage(Main.main.pr + Main.main.setlobby);
				return true;
			}
		} else {
		sender.sendMessage(Main.main.pr + Main.main.nurspieler);
		}
		return false;
	}
	}

}
