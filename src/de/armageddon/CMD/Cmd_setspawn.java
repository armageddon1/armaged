package de.armageddon.CMD;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.armageddon.Main;
import net.md_5.bungee.api.ChatColor;

public class Cmd_setspawn implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sn, Command cmd, String lb, String[] args) {
		if (!sn.hasPermission("paintball.setspawn")) {
			sn.sendMessage(Main.main.pr + Main.main.nopermission);
		} else {
			if (args.length == 0) {
				sn.sendMessage(Main.main.pr + Main.main.errorspawn);
				return false;
			} else {
				if (args.length == 2) {
					if (args[0].equals("0")) {
						Player p = (Player) sn;
						Location loc = p.getLocation();
						String arg = args[1];
						int nm = Integer.parseInt(arg);
						Main.main.lm.setSpawns0(nm, loc);
						p.sendMessage(Main.main.pr + "Erfolgreich einen Spawn f�r " + ChatColor.BLUE + "Blau"
								+ ChatColor.GRAY + " gesetzt!");
					} else if (args[0].equals("1")) {
						Player p = (Player) sn;
						Location loc = p.getLocation();
						String arg = args[1];
						int nm = Integer.parseInt(arg);
						Main.main.lm.setSpawns1(nm, loc);
						p.sendMessage(Main.main.pr + "Erfolgreich einen Spawn f�r " + ChatColor.RED + "Rot "
								+ ChatColor.GRAY + "gesetzt!");
					}
				} else {
					if (sn instanceof Player) {
						Player p = (Player) sn;
						if (p.hasPermission("paintball.setspawn")) {
							if (args[0].equals("0")) {
								Location l = p.getLocation();
								Main.main.lm.setSpawn(0, l);
								p.sendMessage(Main.main.pr + Main.main.setspawn0);
							} else if (args[0].equals("1")) {
								Location l = p.getLocation();
								Main.main.lm.setSpawn(1, l);
								p.sendMessage(Main.main.pr + Main.main.setspawn1);
							} else {
								p.sendMessage(Main.main.pr + Main.main.errorspawn);
							}
						} else {
							p.sendMessage(Main.main.pr + Main.main.nopermission);
						}
					} else {
						sn.sendMessage(Main.main.pr + Main.main.nurspieler);
					}
				}
			}
		}

		return false;
	}

}
